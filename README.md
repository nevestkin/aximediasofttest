### Установка ###
	$ composer install

### Настройка .env файла ###
	Установить DATABASE_URL (doctrine/doctrine-bundle)
	Установить JWT_PASSPHRASE (lexik/jwt-authentication-bundle)
	https://symfony.com/doc/current/configuration.html#config-dot-env

### Генерация SSH ключей ###
	$ mkdir config/jwt
	$ openssl genrsa -out config/jwt/private.pem -aes256 4096
	$ openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

### Создание базы данных ###
	$ bin/console doctrine:database:create
	$ bin/console doctrine:migrations:diff
	$ bin/console doctrine:migrations:migrate

### API ###
	Документация API по адресу http://example.org/api/doc.
	
### Пример регистрации пользователя ###
		$ curl -X POST "http://localhost:8000/api/register" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"test@test.ru\", \"password\": \"test123\"}"
		
### Пример запроса токена ###
		$ curl -X POST "http://localhost:8000/api/token" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"test@test.ru\", \"password\": \"test123\"}"

### Пример получения новостей ###
		$ curl -X GET "http://localhost:8000/api/news" -H "accept: application/json" -H "Authorization: Bearer _ваш_токен_