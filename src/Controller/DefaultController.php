<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @Route(path="")
 *
 * @package App\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->redirectToRoute('admin');
    }
}