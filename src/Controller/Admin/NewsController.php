<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\NewsType;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsController
 *
 * @Route(path="admin/news")
 *
 * @Security("is_granted('ROLE_MODERATOR')")
 */
class NewsController extends Controller
{
    /**
     * @Route("/",  name="admin_news")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $currentPage = $request->get('page', 1);

        $queryBuilder = $em->getRepository(News::class)
            ->createQueryBuilder('t');

        if ($filtersData = $request->get('filter', [])) {
            if (isset($filtersData['title'])) {
                $queryBuilder
                    ->andWhere('t.title LIKE :title')
                    ->setParameter('title', '%' . $filtersData['title'] . '%');

            }
            if (isset($filtersData['content'])) {
                $queryBuilder
                    ->andWhere('t.content LIKE :content')
                    ->setParameter('content', '%' . $filtersData['content'] . '%');
            }
        }

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $currentPage,
            $this->getParameter('admin.count_items')
        );

        return $this->render('admin/news/list.html.twig', [
            'pagination' => $pagination,
            'title' => $filtersData['title'] ?? '',
            'content' => $filtersData['content'] ?? '',
        ]);
    }

    /**
     * @Route("/edit",  name="admin_news_edit")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id', false)) {
            $news = $em->getRepository(News::class)->find($request->get('id'));
        } else {
            $news = new News();
        }
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($news);

            $em->flush();
            $em->clear();
            return $this->redirect($this->generateUrl('admin_news'));
        }

        return $this->render('admin/news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete",  name="admin_news_delete")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function delete(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {
            $user = $em->getRepository(News::class)->find($request->get('id'));
            if ($user instanceof News) {
                $em->remove($user);
                $em->flush();
            }
        }

        return $this->redirectToRoute('admin_news');
    }
}