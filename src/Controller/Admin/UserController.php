<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route(path="admin/user")
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class UserController extends Controller
{
    /**
     * @Route("/",  name="admin_user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $currentPage = $request->get('page', 1);

        $queryBuilder = $em->getRepository(User::class)
            ->createQueryBuilder('t');

        if ($filtersData = $request->get('filter', [])) {
            if (isset($filtersData['username'])) {
                $queryBuilder
                    ->andWhere('t.username LIKE :username')
                    ->setParameter('username', '%' . $filtersData['username'] . '%');

            }
            if (isset($filtersData['email'])) {
                $queryBuilder
                    ->andWhere('t.email LIKE :email')
                    ->setParameter('email', '%' . $filtersData['email'] . '%');
            }
        }

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $currentPage,
            $this->getParameter('admin.count_items')
        );

        return $this->render('admin/user/list.html.twig', [
            'pagination' => $pagination,
            'username' => $filtersData['username'] ?? '',
            'email' => $filtersData['email'] ?? '',
        ]);
    }

    /**
     * @Route("/edit",  name="admin_user_edit")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id', false)) {
            $user = $em->getRepository(User::class)->find($request->get('id'));
        } else {
            $user = new User();
        }
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setUsername($user->getEmail());
            $em->persist($user);

            $em->flush();
            $em->clear();
            return $this->redirect($this->generateUrl('admin_user'));
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete",  name="admin_user_delete")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function delete(Request $request): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id') && $this->getUser()->getId() != $request->get('id')) {
            $user = $em->getRepository(User::class)->find($request->get('id'));
            if ($user instanceof User) {
                $em->remove($user);
                $em->flush();
            }
        }

        return $this->redirectToRoute('admin_user');
    }
}