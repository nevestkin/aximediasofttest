<?php

namespace App\Controller\Api;

use App\Entity\News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Swagger\Annotations as SWG;

class NewsController extends Controller
{
    /**
     * Получение списка новостей.
     *
     * @Route("/api/news", name="api_get_news")
     * @Method("GET")
     *
     * @Security("is_granted('ROLE_LOGGED')")
     *
     * @SWG\Parameter(
     *      type="string",
     *      name="Authorization",
     *      in="header",
     *      required=true,
     *      description="Authorization header",
     *      default="Bearer _you_token_",
     *  )
     *
     * @SWG\Response(
     *      response=401,
     *      description="Invalid token",
     *      examples={
     *          "Token not found": "{code: 401, message: 'JWT Token not found'}",
     *          "Expired token": "{code: 401, message: 'Expired JWT Token'}",
     *      },
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="code", type="integer", description="Error code"),
     *          @SWG\Property(property="message", type="string", description="Error description"),
     *      ),
     *  )
     * @SWG\Response(
     *      response=200,
     *      description="News list",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="string",
     *          ),
     *      ),
     *  )
     *
     * @SWG\Tag(name="News")
     *
     * @return array
     * @throws \HttpException
     */
    public function getNewsAction(): array
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(News::class)->findAll();

        if (!isset($news)) {
            throw new HttpException(400, "Invalid data");
        }

        return $news;
    }
}